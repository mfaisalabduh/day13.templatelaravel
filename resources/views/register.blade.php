<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <!-- <form action="/sapa" method="POST">
        @csrf
        <input type="text" name="nama">
        <input type="submit" value="masuk">


    </form> -->

    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="POST">
        @csrf
        <label> First name: </label> <br><br>
        <input type="text" name="namaawal"><br><br>

        <label> Last name: </label> <br><br>
        <input type="text" name="namaakhir"><br><br>

        <label> Gender: </label> <br><br>
        <input type="radio" name="gender" >Male <br>
        <input type="radio" name="gender" >Female <br>
        <input type="radio" name="gender" >Other <br><br>

        <label>Nationality:</label> <br><br>
        <select name="WN">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select> <br><br>

        <label>Language Spoken: </label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>

        <label>Bio:</label> <br><br>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea><br>

        <input type="Submit" value="Sign Up">
    </form>
    
</body>
</html>