@extends('adminlte.master')

@section('header')
    <h1>table</h1>
@endsection

@section('min-subheader')
<div class="card-header">
    <h3 class="card-title">Table</h3> 
    <div class="card-tools">
            
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
            
          </div>
        </div>
@endsection

@section('content')
<div class="card-body">
    @include('tasks.table')
    </div>
@endsection

<!-- @push('scripts')
<script>
  var wrappers = document.getElementsByClassName("wrappers");

  var items = ["ini","contoh"];

  console.log("ini items: "items)
</script>
@endpush -->