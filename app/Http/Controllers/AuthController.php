<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    // public function sapa_post(Request $request){
    //     // dd($request->all());
    //     $nama = $request["nama"];
    //     return "$nama";
    // }

    public function welcome(Request $request){
        $namaawal = $request["namaawal"];
        $namaakhir = $request["namaakhir"];
        return view('welcome', compact('namaawal','namaakhir'));
    }

    
}
